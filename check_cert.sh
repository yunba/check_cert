#!/bin/bash                                                                                                                   
#YunBa iOS SDK @ SHENZHEN WEIZHIYUN TECHNOLOGY CO.LTD. All rights reserved.
#Dependencies:
#               openssl

#echo "YunBa iOS SDK @ SHENZHEN WEIZHIYUN TECHNOLOGY CO.LTD. All rights reserved."
set -e

usage() {
    echo "Usage: $0 -f \$p12_file_name -i \$pass_in [ -o \$pass_out ]"
    exit $1
}
pass_out=yunba.io
declare -A connect_addr_dict
connect_addr_dict[Development]="gateway.sandbox.push.apple.com:2195"
connect_addr_dict[Production]="gateway.push.apple.com:2195"

## get arguments
while getopts "h?f:i:o:" opt; do #        echo "opt: $opt"
    #echo "OPTARGS: $OPTARG"
    case "$opt" in
        h|\?)
            usage 0
            ;;
        f)  p12_file_name=$OPTARG
            ;;                                                                                                                
        i)  pass_in=$OPTARG
            ;;       
        o)  pass_out=$OPTARG
            ;;
    esac
done

if [ -z ${p12_file_name+x} ] || ! [ -f $p12_file_name ]; then                                              
    usage 1
fi

if [ -z ${pass_in+x} ]; then                                              
    usage 2
fi

if [ -z ${pass_out+x} ]; then                                              
    usage 3
fi

##transform p12 to pem
tmp_pem_file=${p12_file_name}_${BASHPID}_${RANDOM}.pem
rm -f $tmp_pem_file

openssl pkcs12 -in $p12_file_name -out $tmp_pem_file -passin "pass:${pass_in}" -passout "pass:${pass_out}" > /dev/null 2>&1

## read pem file infos
subject_dump=`openssl x509 -in $tmp_pem_file -noout -subject -nameopt RFC2253 `
expre_dump=`openssl x509 -in $tmp_pem_file -noout -enddate`

name=`awk -F":|," '{print $4}' <<< ${subject_dump} | tr -d " "`
env=`awk -F":|," '{print $3}' <<< ${subject_dump} | awk '{print $2}' | tr -d " " `
expire=`awk -F"=" '{print $NF}' <<< ${expre_dump}`

echo ${name}
echo ${env}
echo ${expire}

## connect to apple push service by pem file
if [ -z ${env+x} ] || [ -z ${connect_addr_dict[${env}]+x} ]; then                                              
    echo "connect_addr_dict["${env}"] illegal"
    exit 4
fi

connect_addr=${connect_addr_dict[${env}]}

#ssl_dump=`openssl s_client -connect ${connect_addr} -cert $tmp_pem_file -keyform PEM -pass "pass:${pass_out}" -CAfile $tmp_pem_file -prexit 2>&1` #-debug -showcerts 
#ssl_code=$?

rm ${tmp_pem_file}
exit 0

CERT_KEY=/home/yunba/apns/rel/apns/certs/565fa267f085fc471efe0c57_dev.pem_bad8b091889864f18f9f68ebf459ff29; openssl s_client -connect gateway.sandbox.push.apple.com:2195 -cert $CERT_KEY -debug -showcerts  -CAfile $CERT_KEY
